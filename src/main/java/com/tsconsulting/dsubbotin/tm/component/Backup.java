package com.tsconsulting.dsubbotin.tm.component;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class Backup {

    private final static int INTERVAL = 30_000;

    @NotNull
    private final static String SAVE_COMMAND = "backup-save";

    @NotNull
    private final static String LOAD_COMMAND = "backup-load";

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ExecutorService executorService;

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        executorService = Executors.newSingleThreadExecutor();
    }

    public void start() {
        executorService.execute(() -> {
            while (true) {
                try {
                    Thread.sleep(INTERVAL);
                    save();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void save() {
        bootstrap.runCommand(SAVE_COMMAND);
    }

    public void load() {
        bootstrap.runCommand(LOAD_COMMAND);
    }

}
