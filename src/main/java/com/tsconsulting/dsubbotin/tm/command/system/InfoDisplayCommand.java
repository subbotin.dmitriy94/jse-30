package com.tsconsulting.dsubbotin.tm.command.system;

import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.util.NumberUtil;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class InfoDisplayCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String name() {
        return "info";
    }

    @Override
    @NotNull
    public String arg() {
        return "-i";
    }

    @Override
    @NotNull
    public String description() {
        return "Display system info.";
    }

    @Override
    public void execute() {
        final int processors = Runtime.getRuntime().availableProcessors();
        TerminalUtil.printMessage("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        TerminalUtil.printMessage("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryValue = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat;
        TerminalUtil.printMessage("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        TerminalUtil.printMessage("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        TerminalUtil.printMessage("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
