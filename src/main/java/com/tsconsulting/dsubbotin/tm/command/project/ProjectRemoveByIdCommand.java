package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove project by id.";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws Exception {
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter project id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findById(currentUserId, projectId);
        serviceLocator.getProjectTaskService().removeProjectById(currentUserId, projectId);
        TerminalUtil.printMessage("[Project removed]");
    }

}
