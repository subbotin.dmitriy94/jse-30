package com.tsconsulting.dsubbotin.tm.exception.entity;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public final class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Entity not found!");
    }

}